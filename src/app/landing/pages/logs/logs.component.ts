import { Component, OnInit, Input } from '@angular/core';
import { LogModel } from 'src/app/core/models/log.model';
import { LogService } from 'src/app/core/services/log.service';
import { PdfService } from 'src/app/core/services/pdf.service';

@Component({
    selector: 'app-journal',
    templateUrl: './logs.component.html',
    styleUrls: ['./logs.component.scss']
})
export class LogsComponent implements OnInit {
    @Input() items: LogModel[] = [];
    constructor(private log: LogService, private pdf: PdfService) { }

    ngOnInit() {
        this.log.list().subscribe(items => (this.items = items));
    }

    onClick(item: LogModel) {
        console.log('ITEM = ', item);
        this.pdf.download(item.title);
    }
}
