import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'app-list-items',
    templateUrl: './list-items.component.html',
    styleUrls: ['./list-items.component.scss']
})
export class ListItemsComponent<T> {
    @Input() showArrow = true;
    @Input() items: T[] = [];
    @Output() choose = new EventEmitter();
    constructor() {}

    onClick(item: T) {
        this.choose.emit(item);
    }
}
