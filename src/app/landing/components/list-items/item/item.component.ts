import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.scss']
})
export class ItemComponent<T> {
    @Input() item: T;
    @Input() showArrow: boolean;
    constructor() {}
}
