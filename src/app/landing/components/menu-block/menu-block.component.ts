import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-menu-block',
    templateUrl: './menu-block.component.html',
    styleUrls: ['./menu-block.component.scss']
})
export class MenuBlockComponent {
    @Input() img: string;
    @Input() title: string;
    @Input() description: string;
    @Input() url: string;
    @Input() number: number;

    constructor(private router: Router) {}

    onClick() {
        this.router.navigate([this.url]);
    }
}
