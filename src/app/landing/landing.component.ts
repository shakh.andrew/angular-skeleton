import { Component, OnInit } from '@angular/core';
import { SidebarService } from '../core/services/sidebar.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
    sidebarIsOpened = false;

    constructor(private sidebar: SidebarService, private router: Router) { }

    ngOnInit() {
        this.sidebar.show.subscribe(status => (this.sidebarIsOpened = status));
    }

    onTabClick() {
        this.router.navigate(['landing']);
    }

    closeSlidebar() {
        this.sidebar.close();
    }
}
