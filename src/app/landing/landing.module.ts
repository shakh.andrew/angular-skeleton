import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { LandingComponent } from './landing.component';
import { SharedModule } from '../shared/shared.module';
import { MenuBlockComponent } from './components/menu-block/menu-block.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ListItemsComponent } from './components/list-items/list-items.component';
import { ItemComponent } from './components/list-items/item/item.component';
import { LogsComponent } from './pages/logs/logs.component';

@NgModule({
    declarations: [
        LandingComponent,
        MenuBlockComponent,
        DashboardComponent,
        LogsComponent,
        ListItemsComponent,
        ItemComponent
    ],
    imports: [CommonModule, LandingRoutingModule, SharedModule]
})
export class LandingModule { }
