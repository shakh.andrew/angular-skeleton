import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    constructor(
        private auth: AuthService,
        private router: Router
    ) {
    }

    ngOnInit() {

    }

    login() {
        this.auth.login('token_qwerty').subscribe(() => {
            this.router.navigate(['landing']);
        });
    }
}
