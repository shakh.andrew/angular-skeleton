import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { LoginComponent } from './pages/login/login.component';
import { AuthenticationGuard } from './core/guards/authentication.guard';
import { LoginGuard } from './core/guards/login.guard';


const routes: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login', canActivate: [LoginGuard], component: LoginComponent },
    {
        path: 'landing',
        canActivate: [AuthenticationGuard],
        loadChildren: () => import('./landing/landing.module').then(m => m.LandingModule)
    },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
