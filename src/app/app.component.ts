import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../environments/environment';
import { AuthService } from './core/services/auth.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    isLoggedIn: boolean;

    constructor(private translate: TranslateService, private auth: AuthService) {
        this.translate.setDefaultLang('en');
        this.translate.use(environment.language);
        this.isLoggedIn = this.auth.isLoggedIn;
    }

    ngOnInit() {
        this.auth.currentUser.subscribe(() => (this.isLoggedIn = this.auth.isLoggedIn));
    }
}
