import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../core/services/auth.service';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { SidebarService } from '../../../core/services/sidebar.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    logo = '';

    constructor(
        private auth: AuthService,
        private router: Router,
        private notifier: NotifierService,
        private sidebar: SidebarService
    ) { }

    ngOnInit() {
    }

    logout() {
        this.auth.logout();
        this.router.navigate([`/login`]);
        this.notifier.notify('default', 'See you soon');
    }

    toggle() {
        this.sidebar.setState(!this.sidebar.status);
    }
}
