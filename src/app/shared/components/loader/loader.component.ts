import { Component, OnInit } from '@angular/core';
import { LoaderService } from './loader.service';

@Component({
    selector: 'app-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {
    loading = false;

    constructor(private loader: LoaderService) {}

    ngOnInit() {
        this.loader.showLoader.subscribe((showLoader: boolean) => (this.loading = showLoader));
    }
}
