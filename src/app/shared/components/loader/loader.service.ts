import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class LoaderService {
    private showLoaderSubject: BehaviorSubject<boolean>;
    showLoader: Observable<boolean>;

    constructor() {
        this.showLoaderSubject = new BehaviorSubject<boolean>(false);
        this.showLoader = this.showLoaderSubject.asObservable();
    }

    show(): void {
        this.showLoaderSubject.next(true);
    }

    hide(): void {
        this.showLoaderSubject.next(false);
    }

    get state(): boolean {
        return this.showLoaderSubject.value;
    }
}
