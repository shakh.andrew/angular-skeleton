import { NotifierOptions } from 'angular-notifier';

export const NotifierSettings: NotifierOptions = {
    position: {
        horizontal: {
            position: 'middle',
            distance: 12
        },
        vertical: {
            position: 'bottom',
            distance: 12,
            gap: 10
        }
    }
};
