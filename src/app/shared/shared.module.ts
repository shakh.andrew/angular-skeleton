import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule, NgbTab, NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { NotifierModule } from 'angular-notifier';
import { NotifierSettings } from './configs/notifier';
import { NgxLoadingModule } from 'ngx-loading';
import { LoaderComponent } from './components/loader/loader.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarModule } from 'ng-sidebar';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { DeviceDetectorModule } from 'ngx-device-detector';

@NgModule({
    declarations: [LoaderComponent, HeaderComponent],
    imports: [
        CommonModule,
        NgbModule,
        NotifierModule.withConfig(NotifierSettings),
        NgxLoadingModule.forRoot({}),
        SidebarModule.forRoot(),
        RouterModule,
        DeviceDetectorModule.forRoot()
    ],
    exports: [
        TranslateModule,
        NotifierModule,
        NgxLoadingModule,
        LoaderComponent,
        HeaderComponent,
        NgbModule,
        NgbTabset,
        NgbTab,
        SidebarModule
    ]
})
export class SharedModule { }
