export interface Adapter<TDto, TModel> {
    adapt(item: TDto): TModel;
}
