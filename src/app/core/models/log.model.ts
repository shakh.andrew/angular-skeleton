import { Injectable } from '@angular/core';
import { Adapter } from './adapter';
import * as moment from 'moment';

export class LogDto {
    id: number;
    name: string;
    date: string;
}

export class LogModel {
    id: number;
    title: string;
    date: moment.Moment;

    constructor(id: number = 0, title: string = '', date: string = '') {
        this.id = id;
        this.title = title;
        this.date = moment(date);
    }
}

@Injectable({
    providedIn: 'root'
})
export class LogAdapter implements Adapter<LogDto, LogModel> {
    adapt(item: LogDto): LogModel {
        return new LogModel(item.id, item.name, item.date);
    }
}
