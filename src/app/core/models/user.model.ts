import { Injectable } from '@angular/core';
import { Adapter } from './adapter';

export class UserDto {
    id: number;
    email: string;
    firstName: string;
    lastName: string;
}

export class UserModel {
    id: number;
    email: string;
    firstName: string;
    lastName: string;

    constructor(id: number = 0, email: string = '', firstName: string = '', lastName: string = '') {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}

@Injectable({
    providedIn: 'root'
})
export class UserAdapter implements Adapter<UserDto, UserModel> {
    adapt(item: UserDto): UserModel {
        return new UserModel(item.id, item.email, item.firstName, item.lastName);
    }
}
