import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoaderService } from 'src/app/shared/components/loader/loader.service';
import { LogDto, LogModel, LogAdapter } from '../models/log.model';
import { environment } from 'src/environments/environment';
import { tap, delay, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class LogService {
    constructor(private http: HttpClient, private loader: LoaderService, private adapter: LogAdapter) { }

    list(): Observable<LogModel[]> {
        this.loader.show();

        return this.http.get<LogDto[]>(`${environment.apiUrl}/assets/requests/logs.json`).pipe(
            delay(1000),
            map((items: LogDto[]) => {
                return items.map(item => this.adapter.adapt(item));
            }),
            tap(() => this.loader.hide())
        );
    }
}
