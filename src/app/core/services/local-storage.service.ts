import { Injectable } from '@angular/core';
import { UserModel } from '../models/user.model';

@Injectable({
    providedIn: 'root'
})
export class LocalStorageService {
    constructor() {}

    get<T>(key: string): T | null {
        const ls: string | null = localStorage.getItem(key);
        if (ls) {
            return JSON.parse(ls) as T;
        }
        return null;
    }

    set<T>(key: string, val: T): void {
        localStorage.setItem(key, JSON.stringify(val));
    }

    remove(key: string): void {
        localStorage.removeItem(key);
    }

    getUser(): UserModel {
        const user = this.get<UserModel>('currentUser');
        if (!user) {
            return new UserModel();
        }
        return new UserModel(user.id, user.email, user.firstName, user.lastName);
    }

    setUser(user: UserModel): void {
        this.set<UserModel>('currentUser', user);
    }

    clearUser(): void {
        this.remove('currentUser');
    }
}
