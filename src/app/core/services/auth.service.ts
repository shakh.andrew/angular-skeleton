import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from './local-storage.service';
import { UserModel, UserAdapter, UserDto } from '../models/user.model';
import { environment } from '../../../environments/environment';
import { delay, map, tap } from 'rxjs/operators';
import { LoaderService } from '../../shared/components/loader/loader.service';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private currentUserSubject: BehaviorSubject<UserModel>;
    currentUser: Observable<UserModel>;

    constructor(
        private http: HttpClient,
        private storage: LocalStorageService,
        private loader: LoaderService,
        private adapter: UserAdapter
    ) {
        this.currentUserSubject = new BehaviorSubject<UserModel>(this.storage.getUser());
        this.currentUser = this.currentUserSubject.asObservable();
    }

    get currentUserValue(): UserModel {
        return this.currentUserSubject.value;
    }

    login(token: string = ''): Observable<UserModel> {
        this.loader.show();
        return this.getUser(token).pipe(
            tap(user => {
                this.storage.setUser(user);
                this.currentUserSubject.next(user);
                this.loader.hide();
                return user;
            })
        );
    }

    private getUser(token = ''): Observable<UserModel> {
        //return this.http.post<any>(`${environment.apiUrl}/assets/requests/user.json`, { token })
        return this.http.get<UserDto>(`${environment.apiUrl}/assets/requests/user.json`).pipe(
            map(user => this.adapter.adapt(user)),
            delay(2000)
        );
    }

    logout() {
        this.storage.clearUser();
        this.storage.remove('settings');
        this.currentUserSubject.next(null);
    }

    get isLoggedIn(): boolean {
        return this.currentUserValue && this.currentUserValue.id > 0;
    }

    get isLoggedOut(): boolean {
        return !this.isLoggedIn;
    }
}
