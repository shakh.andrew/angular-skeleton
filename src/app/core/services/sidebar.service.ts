import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SidebarService {
    private showSubject: BehaviorSubject<boolean>;
    show: Observable<boolean>;

    constructor() {
        this.showSubject = new BehaviorSubject<boolean>(false);
        this.show = this.showSubject.asObservable();
    }

    open(): void {
        this.showSubject.next(true);
    }

    close(): void {
        this.showSubject.next(false);
    }

    setState(val: boolean): void {
        this.showSubject.next(val);
    }

    get status(): boolean {
        return this.showSubject.value;
    }
}
