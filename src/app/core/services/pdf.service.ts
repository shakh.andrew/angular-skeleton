import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class PdfService {
    constructor(private http: HttpClient) {}

    download(name = 'file') {
        const url = `${environment.apiUrl}/assets/requests/test.pdf`;
        const headers: HttpHeaders = new HttpHeaders().set('Authorization', '');

        return this.http.get(url, { headers, responseType: 'blob' }).subscribe(response => {
            const file = new Blob([response], {
                type: 'application/pdf'
            });

            FileSaver.saveAs(file, name + '.pdf');
        });
    }
}
